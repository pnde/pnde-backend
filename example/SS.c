#include "user.h"

float receiveRate = 1;
float sendRate = 1;
float workingRate = 0.01;
float ListeningRate = 0.0166;
float InitRate = 0.00001157;
// float nightSleepTime = 0.0055;
// float daySleepTime = 0.0083;
// float nightAwakeTime = 0.0083;
// float dayAwakeTime = 0.0055;
float nightSleepTime = 0.0166;// 1min
float daySleepTime = 0.0083; // 2min
float nightAwakeTime = 0.0083;// 2min
float dayAwakeTime = 0.0166;// 1min
float nightDuration = 0.000023148;
float dayDuration = 0.000023148;
float lowHarvestRate = 0.033;
float middleHarvestRate = 0.05;
float highHarvestRate = 0.1;
float sessionRate = 0.0000001286;
int dailyMsgsNumber = 20;
int BatteryCapacity = 100;
float T1 = 20;
float T2 = 40;

void options()
{
    iopt(IOP_TSMETHOD, VAL_TSUNIF);
    // iopt(IOP_SSMETHOD,VAL_SSSOR);
    iopt(IOP_PR_FULL_MARK, VAL_YES);
    iopt(IOP_PR_MARK_ORDER, VAL_CANONIC);
    iopt(IOP_PR_MC_ORDER, VAL_TOFROM);
    iopt(IOP_PR_MC, VAL_YES);
    iopt(IOP_MC, VAL_CTMC);
    iopt(IOP_PR_PROB, VAL_YES);
    iopt(IOP_PR_RSET, VAL_YES);
    iopt(IOP_PR_RGRAPH, VAL_YES);
    iopt(IOP_ITERATIONS, 2000000);
    iopt(IOP_CUMULATIVE, VAL_NO);
    fopt(FOP_ABS_RET_M0, 0.0);
    fopt(FOP_PRECISION, 0.001);

    /*bufferSize = input("Enter buffer Size: ");
    inputRate = finput("Enter inputRate: ")*/
}

int HeightHarvestGuard()
{
    if (mark("Battery") < BatteryCapacity && mark("Night") == 0 && mark("Summer") == 1)
    // if (mark("Battery") < BatteryCapacity && mark("Summer") == 1)
        return 1;
    else
        return 0;
}

int MiddleHarvestGuard()
{
    if (mark("Battery") < BatteryCapacity && mark("Night") == 0 && (mark("Spring") == 1 || mark("Autumn") == 1))
    // if (mark("Battery") < BatteryCapacity && (mark("Spring") == 1 || mark("Autumn") == 1))
        return 1;
    else
        return 0;
}

int LowHarvestGuard()
{
    if (mark("Battery") < BatteryCapacity && mark("Night") == 0 && mark("Winter") == 1)
    // if (mark("Battery") < BatteryCapacity  && mark("Winter") == 1)
        return 1;
    else
        return 0;
}

int NotStandByGuard()
{
    if (mark("StandBy") == 0)
        return 1;
    else
        return 0;
}

int MsgsExistGuard()
{
    if (mark("Msgs") == 0)
        return 1;
    else
        return 0;
}

int NightSleepGuard()
{
    if (mark("StandBy") == 0 && mark("Day") == 0)
        return 1;
    else
        return 0;
}

int DaySleepGuard()
{
    if (mark("StandBy") == 0 && mark("Night") == 0)
        return 1;
    else
        return 0;
}

int awakeGuard()
{
    if (mark("Battery") >= T2)
        return 1;
    else
        return 0;
}

int DayAwakeGuard()
{
    if (mark("Battery") >= T2 && mark("Night") == 0)
        return 1;
    else
        return 0;
}

int GoSleepGuard()
{
    if (mark("Battery") <= T1 && mark("StandBy") == 0)
        return 1;
    else
        return 0;
}

void net()
{
    place("Msgs");
    place("Buffer");
    place("TransmitedMsg");
    place("StandBy");
    place("Day");
    place("Night");
    place("Battery");
    place("Winter");
    place("Spring");
    place("Summer");
    place("Autumn");

    init("Msgs", dailyMsgsNumber);
    init("Day", 1);
    init("Summer", 1);
    init("Battery", BatteryCapacity);    

    rateval("ToWinter", sessionRate);
    rateval("ToAutumn", sessionRate);
    rateval("ToSpring", sessionRate);
    rateval("ToSummer", sessionRate);
    rateval("awake", 1);

    rateval("Height_Harvest", highHarvestRate);
    rateval("Middle_Harvest", middleHarvestRate);
    rateval("Low_Harvest", lowHarvestRate);
    rateval("Transmit", sendRate);
    rateval("Listening", ListeningRate);
    rateval("Init", InitRate);
    rateval("Gathering", receiveRate);
    rateval("Working", workingRate);
    rateval("ToDay", nightDuration);
    rateval("ToNight", dayDuration);
    imm("Go_Sleep");

    oarc("ToAutumn", "Autumn");
    iarc("ToAutumn", "Summer");

    oarc("ToWinter", "Winter");
    iarc("ToWinter", "Autumn");

    oarc("ToSpring", "Spring");
    iarc("ToSpring", "Winter");

    oarc("ToSummer", "Summer");
    iarc("ToSummer", "Spring");

    oarc("ToDay", "Day");
    iarc("ToDay", "Night");

    oarc("ToNight", "Night");
    iarc("ToNight", "Day");

    moarc("Init", "Msgs", dailyMsgsNumber);
    miarc("Init", "TransmitedMsg", dailyMsgsNumber);

    oarc("Gathering", "Buffer");
    iarc("Gathering", "Msgs");
    iarc("Gathering", "Battery");

    oarc("Transmit", "TransmitedMsg");
    iarc("Transmit", "Buffer");
    iarc("Transmit", "Battery");

    iarc("Listening", "Battery");

    iarc("Working", "Battery");

    oarc("Height_Harvest", "Battery");
    oarc("Middle_Harvest", "Battery");
    oarc("Low_Harvest", "Battery");

    iarc("awake", "StandBy");

    oarc("Go_Sleep", "StandBy");

    guard("Height_Harvest", HeightHarvestGuard);
    guard("Middle_Harvest", MiddleHarvestGuard);
    guard("Low_Harvest", LowHarvestGuard);
    guard("Transmit", NotStandByGuard);
    guard("Listening", NotStandByGuard);
    guard("Init", MsgsExistGuard);
    guard("Working", NotStandByGuard);
    guard("Gathering", NotStandByGuard);
    guard("Go_Sleep", GoSleepGuard);
    guard("awake",awakeGuard);
    priority("Go_Sleep", 10);
}
int assert()
{
    /*printf("Input %i ",mark("Input"));
    printf("ReadyForProcess %i ",mark("ReadyForProcess"));
    printf("ReadyForSend %i ",mark("ReadyForSend"));
    printf("Source %i ",mark("Source"));*/
    // bufferSize = bufferSize+1;

    // bufferSize = input("Enter buffer Size: ")
    if (mark("Winter") + mark("Summer") + mark("Autumn") + mark("Spring") != 1)
    {
        printf("Winter %i\n", mark("Winter") + mark("Summer") + mark("Autumn") + mark("Spring"));
        return (RES_ERROR);
    }
    if (mark("Day") + mark("Night") != 1)
    {
        printf("Day+Night %i\n", mark("Day") + mark("Night"));
        return (RES_ERROR);
    }

    if (mark("Battery") > BatteryCapacity)
    {
        printf("Day+Battery %i\n", mark("Battery"));
        return (RES_ERROR);
    }
    return (RES_NOERR);
}

void ac_init()
{
    pr_net_info();
}

void ac_reach()
{
    pr_rg_info();
}

/*reward rate */

/*double ReadyForProcess() { return ((double)mark("ReadyForProcess")); }
double ReadyForSend() { return ((double)mark("ReadyForSend")); }
double Source() { return ((double)mark("Source")); }*/

void ac_final()
{
    solve(INFINITY);
    // pr_mc_info();
    /*pr_expected("mark(ReadyForProcess)", ReadyForProcess);
    pr_expected("mark(ReadyForSend)", ReadyForSend);
    pr_expected("rate(Source)", Source);*/
    pr_std_average();
}
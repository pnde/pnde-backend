#include "user.h"

float receiveRate = 1;
float sendRate =1;
float workingRate = 0.01;
float ListeningRate = 0.0166;
float InitRate = 0.00001157;
// float nightSleepTime = 0.0055;
// float daySleepTime = 0.0083;
// float nightAwakeTime = 0.0083;
// float dayAwakeTime = 0.0055;
float HarvestRate = 0.054;
int dailyMsgsNumber = 20;
int BatteryCapacity = 100;
float T1 = 20;
float T2 = 40;

void options()
{
    iopt(IOP_TSMETHOD, VAL_TSUNIF);
    // iopt(IOP_SSMETHOD,VAL_SSSOR);
    iopt(IOP_PR_FULL_MARK, VAL_YES);
    iopt(IOP_PR_MARK_ORDER, VAL_CANONIC);
    iopt(IOP_PR_MC_ORDER, VAL_TOFROM);
    iopt(IOP_PR_MC, VAL_YES);
    iopt(IOP_MC, VAL_CTMC);
    iopt(IOP_PR_PROB, VAL_YES);
    iopt(IOP_PR_RSET, VAL_YES);
    iopt(IOP_PR_RGRAPH, VAL_YES);
    iopt(IOP_ITERATIONS, 2000000);
    iopt(IOP_CUMULATIVE, VAL_NO);
    fopt(FOP_ABS_RET_M0, 0.0);
    fopt(FOP_PRECISION, 0.001);

    /*bufferSize = input("Enter buffer Size: ");
    inputRate = finput("Enter inputRate: ")*/
}



int HarvestGuard()
{
    if (mark("Battery") < BatteryCapacity)
    // if (mark("Battery") < BatteryCapacity  && mark("Winter") == 1)
        return 1;
    else
        return 0;
}

int NotStandByGuard()
{
    if (mark("StandBy") == 0)
        return 1;
    else
        return 0;
}

int MsgsExistGuard()
{
    if (mark("Msgs") == 0)
        return 1;
    else
        return 0;
}

int awakeGuard()
{
    if (mark("Battery") >= T2)
        return 1;
    else
        return 0;
}


int GoSleepGuard()
{
    if (mark("Battery") <= T1 && mark("StandBy") == 0)
        return 1;
    else
        return 0;
}

void net()
{
    place("Msgs");
    place("Buffer");
    place("TransmitedMsg");
    place("StandBy");
    place("Battery");

    init("Msgs", dailyMsgsNumber);
    init("Battery", BatteryCapacity);    
    rateval("awake", 1);

    rateval("Harvest", HarvestRate);
    rateval("Transmit", sendRate);
    rateval("Listening", ListeningRate);
    rateval("Init", InitRate);
    rateval("Gathering", receiveRate);
    rateval("Working", workingRate);
    imm("Go_Sleep");


    moarc("Init", "Msgs", dailyMsgsNumber);
    miarc("Init", "TransmitedMsg", dailyMsgsNumber);


    oarc("Gathering", "Buffer");
    iarc("Gathering", "Msgs");
    iarc("Gathering", "Battery");

    oarc("Transmit", "TransmitedMsg");
    iarc("Transmit", "Buffer");
    iarc("Transmit", "Battery");

    iarc("Listening", "Battery");

    iarc("Working", "Battery");

    oarc("Harvest", "Battery");

    iarc("awake", "StandBy");

    oarc("Go_Sleep", "StandBy");

    guard("Harvest", HarvestGuard);
    guard("Transmit", NotStandByGuard);
    guard("Listening", NotStandByGuard);
    guard("Init", MsgsExistGuard);
    guard("Working", NotStandByGuard);
    guard("Gathering", NotStandByGuard);
    guard("Go_Sleep", GoSleepGuard);
    guard("awake",awakeGuard);
    priority("Go_Sleep", 10);
}
int assert()
{
    /*printf("Input %i ",mark("Input"));
    printf("ReadyForProcess %i ",mark("ReadyForProcess"));
    printf("ReadyForSend %i ",mark("ReadyForSend"));
    printf("Source %i ",mark("Source"));*/
    // bufferSize = bufferSize+1;



    if (mark("Battery") > BatteryCapacity)
    {
        printf("Day+Battery %i\n", mark("Battery"));
        return (RES_ERROR);
    }
    return (RES_NOERR);
}

void ac_init()
{
    pr_net_info();
}

void ac_reach()
{
    pr_rg_info();
}

/*reward rate */

/*double ReadyForProcess() { return ((double)mark("ReadyForProcess")); }
double ReadyForSend() { return ((double)mark("ReadyForSend")); }
double Source() { return ((double)mark("Source")); }*/

void ac_final()
{
    solve(INFINITY);
    // pr_mc_info();
    /*pr_expected("mark(ReadyForProcess)", ReadyForProcess);
    pr_expected("mark(ReadyForSend)", ReadyForSend);
    pr_expected("rate(Source)", Source);*/
    pr_std_average();
}
from abc import ABC, abstractmethod


class ModelRunner(ABC):
    @abstractmethod
    def run(self):
        raise NotImplementedError

from django.apps import AppConfig


class EnginesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pnde_server.engines"

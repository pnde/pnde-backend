/* var.h */

/* 
*
*  Last Update: 29-July-96
* 
*  Change History:
*     Date              Who     Reason
*     ------------      ---     ------------------------------------------------------
*     02-Aug-96         RLJ     added Nparm, *Arrparm 
*     21-Oct-96         RLJ     added ParameterName and ParameterValue
*
*/

# ifdef MAIN
# define	EX
# else
# define	EX	extern
# endif

/*************************************************************************
This file contains the declarations for the global variables of the package
*/

/* These two should eventually disappear, see cspli.c */
extern	int     Retval ;
extern	int     Phase;

EX	char	Msg[256];	/* used to write error messages by everybody */

EX	BOOL	DEBUG;		/* set in main to print debugging info */

/*	This variables are set at the beginning using the user file.	*/

EX	int	Direction;
/* VAL_FROMTO, VAL_TOFROM, or VAL_NOVAL, current direction of matrix */

EX	int	Nplace, FNplace;
/* set to 0, the number of places in the net */

EX	int	Ntrans;
/* set to 0, the number of transitions in the net */

EX	int	Nparm;
/* set to 0, the number of parameters in the net */

EX	FPL	*FArrplace;
/* set to NULL, the array of fluid places (user order) */

EX	PL	*Arrplace;
/* set to NULL, the array of places (user order) */

EX	TR	*Arrtrans;
/* set to NULL, the array of transitions (user order) */

EX	PA	*Arrparm;
/* set to NULL, the array of parameters (user order) */

EX	ME	*Pcan;
/* pointer to the list of markings in canonical order */

EX	ME	*Proot;
/* pointer to the AVL-tree markings */

EX	LL	*Vloop;
/* pointer to the list ov vanishing loops */

EX	int	Ntottan;
/* Total number of tangible markings */

EX	int	Ntan;
/* Number of tangible non absorbing markings */

EX	int	Nabs;
/* Number of tangible absorbing markings */

EX	int	Nvan;
/* Number of vanishing markings */

EX	int	Narcs;
/* Number of (reachability graph) marking to marking transitions */

EX	int	Find;
/* First index for the states in the Markov chain */

EX	int	Nstates;
/* Number of states in the Markov chain */

EX	int	Nentries;
/* Number of entries in the Markov chain */

EX      int     dNentries;
/* Number of entries in the derivative Markov chain */

EX	int	Method;
/* Solution method */

EX	int	Method_transient;
/* true if transient analysis will be done, false otherwise */

EX	int	Method_cumulative;
/* true if cumulative analysis will be done, false otherwise */

EX	char	*modelname;
/* name of the model .c file */

EX	FILE	*Outfile;
/* file where the output for the user is printed */

EX	FILE	*Logfile;
/* file where the messages are printed */

EX	int	Verbose;
/* if true indicates that the verbose output on screen is needed */

EX	BOOL	Rebuild_RG;
/* if true indicates that the reachability graph will be rebuild */ 	

EX	int	createDotFile;
/* if yes then a dot file will be created of the Petri net */

EX	double	*Holding;
/* array of the holding times for the tangible markings */

EX	int	Iterations;
/* Iterations for the MC solution */

EX	double	Precision;
/* Precision of the MC solution */

EX	double	*CProb;
/* array of the probabilities for the tangible markings (CTMC) */

EX	double	*Sens;
/* array of the measurement sensitivities taken from CTMC */

EX    double  *Prob_init;
/* array of the initial distribution probabilities for the CTMC */

EX    double  *Cumprob;
/* array of the cumulative probabilities for the markings */

EX	EE	**Qrow;
/* pointer to the rows of the reachability graph */

EX	EE	**Qcol;
/* pointer to the columns of the reachability graph */

EX	EE	**DerQrow;
/* pointer to the rows of the derivative of the transition matrix */

EX	EE	**DerQcol;
/* pointer to the columns of the derivative of the transition matrix */

EX	double	Time;
/*  time value used in computing transient measures */

EX	int	(*halt_cond)();
/*  global guard function */

EX	char	*ParameterName;
/*  global name of parameter */

EX	double	ParameterValue;
/*  global value of parameter */

EX	int	solved;
/*  global flag indicating that the model has been solved (at least once) */

/*======== for fluid spn =============*/


# undef EX



/* 
*  const.h 
*
*  Last Update: 04-Dec-96
* 
*  Change History:
*     Date              Who     Reason
*     ------------      ---     --------------------------------
*     02-Aug-96         RLJ     added PARM_TYPE
*     26-Sep-96         RLJ     added NORM_TYPE
*     04-Dec-96         GFC     added MAX_PARMS
*
*/


typedef enum bool {FALSE = 0, TRUE = 1} BOOL; 

#define	VALUE_ERROR		-2.0	/* illegal rate/prob value */
#define	INFINITY		-1.0	/* infinity */


/* The following macro returns the number of (significative) elements in
an enumerated type. It assume that the last (and nonsignificative) element
of the type XXX is XXX_MAX */

#define NVALUES(enumtype) (enumtype ## _MAX)

/* hard limits */
#define	MAX_PRIO	10000	/* maximum priority for a transition */
#define	MAX_TOKEN	200	/* max number of tokens in a place */
#define	MAX_TOKENplus1	201	/* MAX_TOKEN + 1 */
#define	MAX_CHAR	256	/* max number of different chars */
#define	MAX_ARRAY	1024	/* max n1 or n2 for an array */
#define	MAX_FLOAT	10e+30	/* infinity ... */
#define	MAX_NAME_LENGTH	256	/* for name of places, trans, etc. */
#define	MAX_NAME_SIZE	500	/* to store "name"."i1"."i2" */
#define	MAX_PARMS	8	/* maximum number of parameters allowed */

#define	HSH_SIZE	1229	/* symbol table size */
#define	HSH_FULL	1001	/* too many symbols */
#define	HSH_JUMP	1327	/* used for the overflows */

/* values returned by some functions */
typedef	enum res_type {
	RES_NOERR	= 0,	/* no error encountered */
	RES_ERROR	= -1,	/* error encountered */
	RES_EOF		= 1		/* eof encountered */
} RES_TYPE;

/* values for the type of matrix storage */
typedef	enum mat_type {
	MAT_ROW2COL,	/* entry (i,j) represents row i column j */
	MAT_COL2ROW	/* entry (i,j) represents column i row j */
} MAT_TYPE;

/* values for error, warning, and other messages */
typedef	enum msg_type {
	MSG_EXIT,			/* fatal error, exit right away */
	MSG_ERROR,			/* fatal error, but don't exit right away */
	MSG_WARNING,		/* warning, execution can proceed */
	MSG_PLAIN,			/* not an error, informational message */
	MSG_TYPE_MAX
} MSG_TYPE;

/* values used when comparing two markings */
typedef enum cmp_type {CMP_LT, CMP_GT, CMP_EQ} CMP_TYPE;

/* values used to classify the MC-transitions */
typedef enum mct_type {
	MCT_ABS		= -1,	/* return to M0 form absmark */
	MCT_PTH		= -2,	/* immediate path ending with timed */
	MCT_MUL		= -3	/* multiple paths cause this transition */
} MCT_TYPE;

/* values used to classify symbols */
typedef enum sym_type {
	SYM_PLACE	= 'p',
	SYM_TRANS	= 't',
        SYM_PARM	= 'z',
	SYM_FPLACE      = 'q',
	SYM_NOVAL	= 'X'
} SYM_TYPE;

/* values used to classify the markings */
typedef enum mar_type {
	MAR_TAN		= 't',		/* tangible marking */
	MAR_ABS		= 'a',		/* absorbing marking */
	MAR_VAN		= 'v',		/* vanishing marking */
	MAR_VLO		= 'l',		/* vanishing (in loop) marking */
	MAR_NOVAL	= ' ',		/* type not yet assigned */
	MAR_T		= 'T',		/* TAN or ABS, illegal for ME.type */
	MAR_V		= 'V',		/* VAN or VLO, illegal for ME.type */
	MAR_TV		= 'A'		/* any marking, illegal for ME.type */
} MAR_TYPE;

/* values used to classify transition distributions */
typedef enum dis_type {
	DIS_COX         = '2',          /* Cox2 distribution   */
	DIS_BIN         = 'a',          /* binomial distribution   */
	DIS_BET         = 'b',          /* beta distribution   */
        DIS_CON         = 'c',          /* constant              */
	DIS_GAM         = 'd',          /* gamma distribution   */
	DIS_EXP		= 'e',		/* exponential           */
	DIS_DEF         = 'f',          /* defective distribution   */
	DIS_GEO         = 'g',          /* geometric distribution*/
	DIS_HYR         = 'h',          /* hyperexponential distribution   */
	DIS_IMM		= 'i',		/* immediate, (time = 0) */
	DIS_HYO         = 'j',          /* hypoexponential distribution   */
	DIS_LOG         = 'l',          /* lognormal distribution*/
	DIS_NBI         = 'm',          /* negative binomial distribution   */
	DIS_NOM         = 'n',          /* normal distribution   */
	DIS_CAU         = 'o',          /* Cauchy distribution   */
	DIS_POI         = 'p',          /* Poisson distribution   */
	DIS_PAR         = 'q',          /* Pareto distribution   */
	DIS_ERL         = 'r',          /* erlang distribution   */
    DIS_UNF         = 'u',          /* uniform distribution  */
	DIS_WEI         = 'w',          /* weibull distribution  */
	DIS_LGI         = 'k',          /* loglogistic distribution  */
	DIS_TRI         = '3',          /* weibull distribution  */
	DIS_NOVAL	= 'X'
} DIS_TYPE;

/* values used to classify marking dependency of transition */
typedef enum dep_type {
	DEP_NO		= 'n',		/* constant */
	DEP_PL		= 'p',		/* infinite server on one place */
	DEP_MK		= 'm'		/* general marking dependent */
} DEP_TYPE;

/* values used to classify resampling/memory policy  */
typedef enum pol_type {
        PRI             = 'i',           /* preemptive repeat identical  */
        PRD             = 'd',           /* preemptive repeat different  */   
        PRS             = 's'            /* preemptive resume            */
} POL_TYPE;

/* values used to indicate whether a (rate) parameter has been defined */
typedef enum parm_type {
	PARM_VAL	= 'v',		/* paramter defined */
	PARM_NOVAL	= 'X'
} PARM_TYPE;

/* values used to classify arcs */
typedef enum arc_type {
	ARC_I		= 'i',		/* input arc */
	ARC_O		= 'o',		/* output arc */
	ARC_H		= 'h',		/* inhibitor arc */
	ARC_NOVAL	= 'X',
	VARIABLE	= MAX_TOKENplus1	/* != from any multiplicity */
} ARC_TYPE;

/* values used to indicate type of normalization needed for solution vector */
typedef enum norm_type {
	DoNotNormalize, 
	SumToZero, 
	SumToOne 
} NORM_TYPE;

/* underflow value used in uniformization */
#define        UNDFLOW 1.0e-30

/* Values used in Fox-Glynn method for poisson probabilities

omega = Highest number the machine can store, I use 1.e+30, but a right
number needs to be found. However, it should not change the results
perceptibly.    
 
tau = Lowest number the machine can store. I use 1.e-30, but again a
right number needs to be found. Again, it shouldn't affect the results much. 
*/

#define TAU 1.e-30
#define OMEGA 1.e+30
#define PI 3.14159265358979323846

/* number of entries allocated at the same time,
NeXT manual says multiples of 8192 are efficient */

#define	ALL_SIZE		8192
#define	ALL_SIZE_MINUS_ONE	8191		/* ALL_SIZE - 1 */



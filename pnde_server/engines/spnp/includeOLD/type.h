/* 
*  type.h 
*
*  Last Update: 29-July-96
* 
*  Change History:
*     Date              Who     Reason
*     ------------      ---     ------------------------------------------------------
*     02-Aug-96         RLJ     added SYM_PARM, struct PA 
*     07-Aug-96         RLJ     added typedef ParmStore for parameter mask in MEs 
*     19-Sep-96         RLJ     added "int entries" to RPCI data structure   
*     12-May-97         GLI     added "something for FSPN "
*/

enum	special_events {
	EVENT_NONE = -10,
	EVENT_PATH = -20,
	EVENT_MERGED = -30,
	EVENT_ERROR = -40
};

typedef unsigned char ParmStore;

typedef int EVENT;

typedef double RATE;

typedef struct rpci {
	MAT_TYPE	order;	/* MAT_ROW2COL or MAT_COL2ROW                    */
	int		entries;/* number of entries                             */
	int		first;	/* index of first row/col                        */
	int		last;	/* index of last row/col                         */
	int		*IP;	/* pointers to start of each row                 */
	int		*JA;	/* indexes of columns                            */
	double		*A;	/* nonzero entries, except possibly the diagonal */
	double		*diag;	/* diagonal entries, if stored separately        */
}	RPCI;

/* 
*	VE: variable element
*	used to store a pointer to a function returning an integer 
*/
typedef int (* VE)();

/* 
*	VA: variable array
*	used to store an array of pointers to functions returning integers 
*/
typedef VE	*VA;

/* 
*	ST: symbol type
*	used to store a symbol (place, transition, parameter) in the symbol table 
*/
typedef struct st {

short	numb;			/* index in the Arrplace, Arrtrans, Arrparm  */
	SYM_TYPE	type;	/* SYM_PLACE, SYM_TRANS, SYM_PARM, SYM_NOVAL */
	char		*symb;	/* pointer to the symbol string              */
}	ST;

/* 
*	PP: place position
*	used to store the number of tokens in a place, used in an array 
*/
typedef	unsigned char	PP;
typedef double FPP;
/* 
*	PL: place type.
*	Used to store a place 
*/
typedef struct pl {
char	*name;		/* name of the place               */
PP	initial;	/* initial number of tokens        */
char	num;		/* number of the place, user order */
struct	pl *pnext;	/* pointer to the next place       */
}	PL;


/* 
*	PA: parameter type.
*	Used to store a parameter 
*/
typedef struct pa {
char      *name;	/* name of the paramter                */
char      num;		/* number of the parameter, user order */
double    val; 		/* value assigned to the parameter     */
PARM_TYPE type;		/* indicates assignment of parameter   */
} PA;


/* 
*	EE: entry element type.
*	Used to store graph arcs, and subsequentely the transition matrix 
*/
typedef struct	ee {
char	tr;		/* number of the PN-transition (user order)  */
RATE	value;		/* rate or probability                       */
   union {
	int	ind;	/* index of the row                          */
	struct	ee *pt;	/* pointer to the next element in the row    */
   } row;
   union {
	int	ind;	/* index of the column                       */
	struct	ee *pt;	/* pointer to the next element in the column */
	struct	me *mk;	/* "mk->index" is equivalent to "ind"        */
   } col;
} EE;


/* 
*	ME: stored marking element type.
*	Used to store the AVL tree, and the pointers to the graph arcs (EE entries) 
*/
typedef struct	me {
int	index;		/* marking index                                              */
struct	me	*pcan;	/* pointer to next marking in canonical order                 */
struct	me	*plr[2];/* pointers to two children in AVL tree                       */
char	bal;		/* LT,GT,EQ: balance of the AVL node                          */
char	type;		/* TAN_M,ABS_M,REP_M,VAN_M,VLO_M,ALO_M: marking type          */
PP	*mk;		/* array representing number of tokens in each place          */
FPP     *fmk;           /* array representing level of fluid in each fluid place      */
FPP     *ratea;         /* array representing net flow rate A(m) in each fluid place  */
FPP     *rateb;         /* array representing net flow rate B(m) in each fluid place  */ 
ParmStore	parms;	/* bit-wise indication of parameters relevant to this marking */
   union {
	EE	*prow;	/* pointer to the row of entries                              */
	EVENT	first;	/* first enabled event                                        */
   } arcs;
} ME;


/* 
*	LL: loop list type.
*	Used to store loops of vanishing markings during the reach set construction 
*/
typedef struct	ll {
struct	me 	*first;	/* pointer to the first marking of the loop */
int	length;		/* number of markings in the loop           */
struct	ll 	*pnext;	/* pointer to the next loop                 */
}	LL;

/* end type.h */
/*=========== add something for FSPN===========*/


typedef struct fbk {
  double val;
  struct fbk *next;
} FBK;


/* 
*	FPL: fluid place type.
*	Used to store a place 
*/
typedef struct fpl {
char	*name;		/* name of the place               */
FPP	initial,bound;	/* initial number of level and bound     */
char	num;		/* number of the fplace, user order */
FBK *fbreak;  /* a list for break point for fluid place */
struct	fpl *pnext;	/* pointer to the next place       */
}	FPL;


typedef double (* FVE)();

/* 
*	FVA: variable array
*	used to store an array of pointers to functions returning double 
*/
typedef FVE	*FVA;

/* 
*	Used to store a transition 
*/
typedef struct tr {
	char		*name;			/* name of the transition                     */
	short		num;			/* number of the transition, user order       */
	short		prio;			/* priority                                   */
	DIS_TYPE	type;			/* distribution type                          */
	DEP_TYPE	dep;			/* dependency type                            */
	int		pl,fpl;			/* index of place, used if DEP_PL             */
        int             policy;                 /* resampling/memory policy                   */
        PP              *affect;                /* enabling resampling policy                 */ 
	PP		*inp;			/* constant input array                       */
        FPP             *finp;                  /* constant impulse input array               */
        FPP             *finr ;                 /* constant input fluid rate  array           */
	VA		vin;			/* marking dependent input array              */
        FVA             fvin;                   /* marking dependent inpulse input array      */
        FVA             fvina;                  /* marking dependent input fluid A(m) array   */
        FVA             fvinb;                  /* marking dependent input fluid B(m) array   */
	PP		*out;		     	/* constant output array                      */
        FPP             *fout;                  /* constant impulse output array              */
        FPP             *foutr;                 /* constant output fluid rate array           */
  	VA		vou;			/* marking dependent output array             */
        FVA             fvou;                   /* marking dependent impulse output array     */
        FVA             fvoua;                  /* marking dependent output fluid A(m) array  */ 
        FVA             fvoub;                  /* marking dependent output fluid B(m) array */ 
	PP		*inh;			/* constant inhibition array                  */
        FPP             *finh;                  /* constant inhibition fluid array            */
	VA		vih;			/* marking dependent inhibition array         */
        FVA             fvih;                   /* marking dependent inhibition fluid array   */  
 	int		(*guard_ptr)();		/* pointer to the guard function              */
	double		val,val2,val3,val4;		/* constant prob/rate value, if any           */
	double		(*fun)(),(*fun2)(),(*fun3)(),(*fun4)();	/* pointer to the prob/rate function          */
	short		totparm;		/* number of (rate) parameters                */
	short 		*parm;			/* parm list(of indices into parameter array) */
        double          prob_val;               /* deal with probility                        */ 
        DEP_TYPE        prob_dep;
        int             prob_pl,prob_fpl;
        double          (*prob_fun)();          /* choose transitions which fire at same time */
	struct		tr *pnext;		/* pointer to the next transition             */
}	TR;











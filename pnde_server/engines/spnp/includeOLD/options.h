/* options.h */

#define MAX_LEVEL_SPLIT 30

double FOP_SPLIT_THRESHOLDS[MAX_LEVEL_SPLIT+1];

typedef enum iop_type {				/* values used for integer options */

IOP_ELIMINATION,
/* vanishing markings elimination: VAL_REDONTHEFLY,VAL_REDAFTERRG,VAL_REDNEVER */

IOP_PR_MARK_ORDER,
/* order when printing the reach set: VAL_CANONIC,VAL_LEXICAL,VAL_MATRIX */

IOP_PR_MERG_MARK,
/* merge vanishing and tangible markings in printing rset: VAL_YES,VAL_NO */

IOP_PR_FULL_MARK,
/* print the markings in full format: VAL_YES,VAL_NO */

IOP_PR_RSET,
/* print the reach set: VAL_YES,VAL_NO,VAL_TAN */

IOP_PR_RGRAPH,
/* print the reach graph: VAL_YES,VAL_NO */

IOP_PR_MC,
/* print the Markov Chain: VAL_YES,VAL_NO */

IOP_PR_DERMC,
/* print the derivative Markov Chain: VAL_YES,VAL_NO */

IOP_PR_MC_ORDER,
/* order when printing the Markov Chain: VAL_FROMTO,VAL_TOFROM */

IOP_PR_PROB,
/* print the tangible marking probas: VAL_YES,VAL_NO */

IOP_PR_PROBDTMC,
/* print the DTMC state probs (used if IOP_MC==VAL_DTMC): VAL_YES,VAL_NO */

IOP_PR_DOT,
/* print the dot graph language description of the Petri net: VAL_YES,VAL_NO */

IOP_MC,
/* type of the Markov Chain: VAL_DTMC,VAL_CTMC */

IOP_OK_ABSMARK,
/* absorbing markings are acceptable: VAL_YES,VAL_NO */

IOP_OK_VANLOOP,
/* loops of vanishing markings are acceptable: VAL_YES,VAL_NO */

IOP_OK_TRANS_M0,
/* a transient initial marking is acceptable: VAL_YES,VAL_NO */

IOP_OK_VAN_M0,
/* a transient initial marking is acceptable: VAL_YES,VAL_NO */

IOP_DEBUG,
/* output useful info during rgraph construction: VAL_YES,VAL_NO */

IOP_USENAME,
/* use the name instead of the number when printing reach graph info */

IOP_TSMETHOD,
/* the transient-state solution method: VAL_TSUNIF,VAL_FOXUNIF */

IOP_SSMETHOD,
/* the steady-state solution method: VAL_GASEI,VAL_SSSOR,VAL_POWER*/

IOP_ITERATIONS,
/* maximum number of iterations for the solution method */

IOP_SENSITIVITY,
/* print the Markov chain state probabilities: VAL_YES,VAL_NO */

IOP_CUMULATIVE,
/* print the Markov chain accumulated time spent in states: VAL_YES,VAL_NO */

IOP_SSDETECT,
/* Use steady-state detection in transient analysis: VAL_YES,VAL_NO */

/* following parts are defined for simulation */

IOP_SIMULATION,
/* simulation */

IOP_SIM_RUNS,
/* number of iterations to run, 0 if FOP_SIM_ERROR is defined */

IOP_SIM_RUNMETHOD,
/* procedure to get the simulation runs: VAL_REPL, VAL_BATCH, VAL_IS, 
VAL_RESTART, VAL_SPLIT, VAL_THIN, VAL_BATHIN, VAL_ISTHIN, VAL_REG, VAL_ISREG */

IOP_SIM_SEED,
/* seed of random number generator, 528361746 as the default */

IOP_SIM_CUMULATIVE,
/* make the data collection method over time if VAL_YES default*/

IOP_SIM_STD_REPORT,
/* print the standard report to .out file in simulation: VAL_YES,VAL_NO */

IOP_SPLIT_LEVEL_DOWN, 
 
IOP_SPLIT_PRESIM,

IOP_SPLIT_NUMBER,

IOP_SPLIT_RESTART_FINISH,

IOP_SPLIT_PRESIM_RUNS,

IOP_TYPE_MAX
/* number of integer options */

} IOP_TYPE;



typedef enum fop_type {				/* values for floating point options */

FOP_ABS_RET_M0		= IOP_TYPE_MAX,
/* rate from absorbing markings to initial marking, if positive */

FOP_PRECISION,
/* required precision for the numerical solution */

FOP_SSPRES,
/* required precision for the steady state detection in tsunif */

/*some options for simulation */

FOP_SIM_LENGTH,
/* length of simulation */

FOP_SIM_CONFIDENCE,
/* confidence interval */

FOP_SIM_ERROR,
/* target error required before simulation stops */

FOP_FLUID_EPSILON,
/* used for fluid logic condition */

FOP_TIME_EPSILON,
/* used for comparing firing conflict */

FOP_TYPE_MAX
/* number of floating point options */

} FOP_TYPE;

/* predefined values for the ioptions */
enum VAL_TYPE {VAL_NO, VAL_YES, VAL_TAN, VAL_CANONIC, VAL_LEXICAL,
		VAL_MATRIX, VAL_TOFROM, VAL_FROMTO, VAL_GASEI, VAL_SSSOR, VAL_TSUNIF,
		VAL_DTMC, VAL_CTMC, VAL_NOVAL, VAL_FOXUNIF, VAL_POWER, VAL_REPL,VAL_BATCH, VAL_IS, 
	       VAL_RESTART, VAL_SPLIT, VAL_THIN, VAL_BATHIN, VAL_ISTHIN, VAL_REG, VAL_ISREG,
		VAL_REDONTHEFLY,VAL_REDAFTERRG,VAL_REDNEVER};

int		IOptionValue(IOP_TYPE opt);
double	FOptionValue(FOP_TYPE opt);
void	iopt(IOP_TYPE opt, int val);
void	fopt(FOP_TYPE opt, double val);











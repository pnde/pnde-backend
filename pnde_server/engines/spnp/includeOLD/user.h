/* user.h */
/* BT: Oct 29  1999:  4 parameters for distributions */

#include	"sysinclude.h"
#include	"port.h"
#include	"const.h"
#include	"type.h"
#include	"var.h"
#include	"options.h"

#ifdef __cplusplus
   extern "C" {
#endif

double          newmtta(double (*func)(void));

void            pr_newmtta(char* string);


void            solve(double time); /* numerical solution */
void            sens(void);             /* sens((char *),...); has variable arg list of (char *) */
void            rebuild(void);      /* rebuild reachability graph at the next solve() */

int		i_dum(void) 	{return(0);}	/* to avoid lint complaints */
double	        d_dum(void)	        {return(0.0);}	/* to avoid lint complaints */


/* CSPL */
double		get_double(char *filename);
void		get_doubles(char *filename, int num, double *values);
void		put_double(char *filename, double value, int sigDigits);
void		put_doubles(char *filename, int num, double *values, int sigDigits);
void		place(char *name);
void		init(char *name, int n);
/* void		trans(char *name); */
void		priority(char *name, int prio);
void		guard(char *name, int (*gfunc)());
void		halting_condition(int (*gfunc)());
void		distr(int type,char *name,int dep,double val,double val2,
                double val3,double val4,char *pl,double (*g)(),double (*g2)(),
                double (*g3)(),double (*g4)());
void            prob(char *name,int dep,double val,char *pl,double (*fun)());
void		arc(int type, char *tr, char *pl, int mult, int (*func)());
void   		parm(char *name);
void		parmtran(char *tr, char *name);
void		bind(char *name, double val);
char 		*AllOfThem(void);
void		pr_parms();
int			input(char *descr);
double		finput(char *descr);
int			mark(char *pl);
int			enabled(char *tr);
double		rate(char *tr);
void		pr_net_info(void);
double		expected(double (*func)());
double		cum_expected(double (*func)());
void		pr_std_average(void);
void		pr_std_cum_average(void);
void		pr_rg_info(void);
void		pr_mc_info(void);
void		pr_expected(char *string, double (*func)());
void		pr_cum_expected(char *string, double (*func)());
void		pr_time_avg_expected(char *string, double (*func)());
double		accumulated(double (*func)(), double (*func1)());
void		pr_accumulated(char *string, double (*func)());
double 		stateType(void);
double		mtta( double (*func)());
void		pr_mtta(char *string);
void		pr_mtta_fun(char *string, double (*func)());
double		cum_abs(double (*func)());
void		pr_cum_abs(char *string, double (*func)());
void		hold_cond(int (*cond)(), double on_off[2]);
void		pr_hold_cond(char *string, int (*cond)());
void		set_prob0(int scale, double (*func)());
void		set_prob_init(double (*func)());
void		pr_value(char *string, double value);
void		pr_message(char *string);

void		fplace(char *name);
void		finit(char *name, double v);
void            fbreak(char *name,double v);
void		fbound(char *name, double v);
void    	farc(int type, char *tr,  char *pl, double mult, double (*func)(),double (*g)());
void    	darc(int type, char *tr,  char *pl, double mult, double (*func)());
BOOL            InternalEnabled(TR *tr);
void            finfinity(char *name);
int             fcondition(char *pl,char *rel,double v1);
double          fmark(char *fpl); 
void            policy(char *t, int pol);
void            affected(char *t,char *y,int pol);

/* for SPLITTING */


int     findplace_without_msg(char *name);
int     findplace(char *name);
int     findfplace(char *name);


/* for Importance Sampling */


void resampling(int (*gfunc)());
void distr_IS(int type,char *name,int dep,double val,double val2,
              double val3,double val4,char *pl,double (*g)(),double (*g2)(),
              double (*g3)(),double (*g4)());
void  prob_IS(char *name,int dep,double val,char *pl,double (*fun)());

#ifdef __cplusplus
}  /*CPLUSPLUS bracket*/
#endif


/* use the following predefined functions to define the firing time distributions */
/* (exactly one of these functions must be used for each transition) :            */
 
#define inf(s)              distr(DIS_CON,s,DEP_NO,-1,0,0,0,NULL,d_dum,d_dum,d_dum,d_dum)  
#define imm(s)              distr(DIS_IMM,s,DEP_NO,1.0,0,0,0,NULL,d_dum,d_dum,d_dum,d_dum)

#define trans(s)            distr(DIS_IMM,s,DEP_NO,1.0,0,0,0,NULL,d_dum,d_dum,d_dum,d_dum)

#define	rateval(s,v)	    distr(DIS_EXP,s,DEP_NO,v,0,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	ratedep(s,v,p)	    distr(DIS_EXP,s,DEP_PL,v,0,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	ratefun(s,f)	    distr(DIS_EXP,s,DEP_MK,0,0,0,0, NULL,f,d_dum,d_dum,d_dum)

#define	detval(s,v)	    distr(DIS_CON,s,DEP_NO,v,0,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	detdep(s,v,p)	    distr(DIS_CON,s,DEP_PL,v,0,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	detfun(s,f)	    distr(DIS_CON,s,DEP_MK,0,0,0,0, NULL,f,d_dum,d_dum,d_dum)

#define	unifval(s,v,u)	    distr(DIS_UNF,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	unifdep(s,v,u,p)    distr(DIS_UNF,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	uniffun(s,f,g)	    distr(DIS_UNF,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	geomval(s,v,u)	    distr(DIS_GEO,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	geomdep(s,v,u,p)    distr(DIS_GEO,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	geomfun(s,f,g)	    distr(DIS_GEO,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	poisval(s,v,u)	    distr(DIS_POI,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	poisdep(s,v,u,p)    distr(DIS_POI,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	poisfun(s,f,g)	    distr(DIS_POI,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	binoval(s,v,u,w)	    distr(DIS_BIN,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	binodep(s,v,u,w,p)    distr(DIS_BIN,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	binofun(s,f,g,h)	    distr(DIS_BIN,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	negbval(s,v,u,w)	    distr(DIS_NBI,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	negbdep(s,v,u,w,p)    distr(DIS_NBI,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	negbfun(s,f,g,h)	    distr(DIS_NBI,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	hypoval(s,v,u,w,x)	    distr(DIS_HYO,s,DEP_NO,v,u,w,x, NULL,d_dum,d_dum,d_dum,d_dum)
#define	hypodep(s,v,u,w,x,p)    distr(DIS_HYO,s,DEP_PL,v,u,w,x, p,   d_dum,d_dum,d_dum,d_dum)
#define	hypofun(s,f,g,h,i)	    distr(DIS_HYO,s,DEP_MK,0,0,0,0, NULL,f,g,h,i)

#define	hyperval(s,v,u,w)	    distr(DIS_HYR,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	hyperdep(s,v,u,w,p)    distr(DIS_HYR,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	hyperfun(s,f,g,h)	    distr(DIS_HYR,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	cox2val(s,v,u,w)	    distr(DIS_COX,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	cox2dep(s,v,u,w,p)    distr(DIS_COX,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	cox2fun(s,f,g,h)	    distr(DIS_COX,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	defval(s,v,u)	    distr(DIS_DEF,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	defdep(s,v,u,p)    distr(DIS_DEF,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	deffun(s,f,g)	    distr(DIS_DEF,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	weibval(s,v,u)	    distr(DIS_WEI,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	weibdep(s,v,u,p)    distr(DIS_WEI,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	weibfun(s,f,g)	    distr(DIS_WEI,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	normval(s,v,u)	    distr(DIS_NOM,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	normdep(s,v,u,p)    distr(DIS_NOM,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	normfun(s,f,g)	    distr(DIS_NOM,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	lognval(s,v,u)	    distr(DIS_LOG,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	logndep(s,v,u,p)    distr(DIS_LOG,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	lognfun(s,f,g)	    distr(DIS_LOG,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	erlval(s,v,u)	    distr(DIS_ERL,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	erldep(s,v,u,p)     distr(DIS_ERL,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	erlfun(s,f,g)	    distr(DIS_ERL,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	betval(s,v,u)	    distr(DIS_BET,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	betdep(s,v,u,p)     distr(DIS_BET,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	betfun(s,f,g)	    distr(DIS_BET,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	gamval(s,v,u)	    distr(DIS_GAM,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	gamdep(s,v,u,p)     distr(DIS_GAM,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	gamfun(s,f,g)	    distr(DIS_GAM,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	parval(s,v,u)	    distr(DIS_PAR,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	pardep(s,v,u,p)     distr(DIS_PAR,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	parfun(s,f,g)	    distr(DIS_PAR,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	cauval(s,v,u)	    distr(DIS_CAU,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	caudep(s,v,u,p)     distr(DIS_CAU,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	caufun(s,f,g)	    distr(DIS_CAU,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	trival(s,v,u,w)	    distr(DIS_TRI,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	tridep(s,v,u,w,p)    distr(DIS_TRI,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	trifun(s,f,g,h)	    distr(DIS_TRI,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	llgival(s,v,u,w)	    distr(DIS_LGI,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	llgidep(s,v,u,w,p)    distr(DIS_LGI,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	llgifun(s,f,g,h)	    distr(DIS_LGI,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)


#define probval(s,v)        prob(s,DEP_NO,v,NULL,d_dum)
#define	probdep(s,v,p)	    prob(s,DEP_PL,v,  p, d_dum)
#define	probfun(s,f)	    prob(s,DEP_MK,0,NULL,f)

#define	useparm(t,s)	parmtran(t,s)
#define ALL			(char *)AllOfThem()
#define	iarc(a,b)		arc(ARC_I,a,b,1,       i_dum)
#define	oarc(a,b)		arc(ARC_O,a,b,1,       i_dum)
#define	harc(a,b)		arc(ARC_H,a,b,1,       i_dum)
#define	miarc(a,b,m)	arc(ARC_I,a,b,m,       i_dum)
#define	moarc(a,b,m)	arc(ARC_O,a,b,m,       i_dum)
#define	mharc(a,b,m)	arc(ARC_H,a,b,m,       i_dum)
#define	viarc(a,b,f)	arc(ARC_I,a,b,VARIABLE,f)
#define	voarc(a,b,f)	arc(ARC_O,a,b,VARIABLE,f)
#define	vharc(a,b,f)	arc(ARC_H,a,b,VARIABLE,f)

/* for fluid rate arc--pieacewise constant  */
#define	fiarc(a,b)		farc(ARC_I,a,b,1.0, d_dum, d_dum)
#define	foarc(a,b)		farc(ARC_O,a,b,1.0, d_dum, d_dum)
#define	fmiarc(a,b,m)	farc(ARC_I,a,b,m,       d_dum, d_dum)
#define	fmoarc(a,b,m)	farc(ARC_O,a,b,m,       d_dum, d_dum)
#define	fviarc(a,b,f)	farc(ARC_I,a,b,INFINITY,d_dum, f )
#define	fvoarc(a,b,f)	farc(ARC_O,a,b,INFINITY,d_dum, f )

/* for fluid rate arc--linear equation */
#define fliarc(a,b,f,g) farc(ARC_I,a,b,INFINITY,f,g)
#define floarc(a,b,f,g) farc(ARC_O,a,b,INFINITY,f,g)

/* for fluid impulse arc */
#define	diarc(a,b)		darc(ARC_I,a,b,1.0,       d_dum)
#define	doarc(a,b)		darc(ARC_O,a,b,1.0,       d_dum)
#define	dharc(a,b)		darc(ARC_H,a,b,1.0,       d_dum)
#define	dmiarc(a,b,m)	darc(ARC_I,a,b,m,       d_dum)
#define	dmoarc(a,b,m)	darc(ARC_O,a,b,m,       d_dum)
#define	dmharc(a,b,m)	darc(ARC_H,a,b,m,       d_dum)
#define	dviarc(a,b,f)	darc(ARC_I,a,b,INFINITY,f)
#define	dvoarc(a,b,f)	darc(ARC_O,a,b,INFINITY,f)
#define	dvharc(a,b,f)	darc(ARC_H,a,b,INFINITY,f)
/* end of fluid arc */
/*---------------------------------------------
Wrong macro?
#define	set_prob0(f)	setProb(VAL_NO, f)
#define	scale_prob0(f)	setProb(VAL_YES,f)
----------------------------------------------*/
/* define relation to fluid place conditions */

#define F_EQ     "=="
#define F_NE     "!="
#define F_LT     "<"
#define F_LQ     "<="
#define F_GT     ">"
#define F_GE     ">="

/* for importance sampling */

#define rateval_is(s,v)        distr_IS(DIS_EXP,s,DEP_NO,v,0,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define ratedep_is(s,v,p)      distr_IS(DIS_EXP,s,DEP_PL,v,0,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define ratefun_is(s,f)        distr_IS(DIS_EXP,s,DEP_MK,0,0,0,0, NULL,f,d_dum,d_dum,d_dum)


/* not included */
/*  #define geomval_is(s,v,u)      distr_IS(DIS_GEO,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define geomdep_is(s,v,u,p)    distr_IS(DIS_GEO,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define geomfun_is(s,f,g)      distr_IS(DIS_GEO,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	poisval_is(s,v,u)      distr_IS(DIS_POI,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	poisdep_is(s,v,u,p)    distr_IS(DIS_POI,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	poisfun_is(s,f,g)      distr_IS(DIS_POI,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	binoval_is(s,v,u,w)	 distr_IS(DIS_BIN,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	binodep_is(s,v,u,w,p)    distr_IS(DIS_BIN,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	binofun_is(s,f,g,h)	 distr_IS(DIS_BIN,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum) */

#define	unifval_is(s,v,u)      distr_IS(DIS_UNF,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	unifdep_is(s,v,u,p)    distr_IS(DIS_UNF,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	uniffun_is(s,f,g)      distr_IS(DIS_UNF,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	weibval_is(s,v,u)      distr_IS(DIS_WEI,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	weibdep_is(s,v,u,p)    distr_IS(DIS_WEI,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	weibfun_is(s,f,g)      distr_IS(DIS_WEI,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	erlval_is(s,v,u)       distr_IS(DIS_ERL,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	erldep_is(s,v,u,p)     distr_IS(DIS_ERL,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	erlfun_is(s,f,g)       distr_IS(DIS_ERL,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	parval_is(s,v,u)       distr_IS(DIS_PAR,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	pardep_is(s,v,u,p)     distr_IS(DIS_PAR,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	parfun_is(s,f,g)       distr_IS(DIS_PAR,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	cauval_is(s,v,u)       distr_IS(DIS_CAU,s,DEP_NO,v,u,0,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	caudep_is(s,v,u,p)     distr_IS(DIS_CAU,s,DEP_PL,v,u,0,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	caufun_is(s,f,g)       distr_IS(DIS_CAU,s,DEP_MK,0,0,0,0, NULL,f,g,d_dum,d_dum)

#define	hyperval_is(s,v,u,w)	    distr_IS(DIS_HYR,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	hyperdep_is(s,v,u,w,p)    distr_IS(DIS_HYR,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	hyperfun_is(s,f,g,h)	    distr_IS(DIS_HYR,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

//#define	cox2val_is(s,v,u,w)	    distr_IS(DIS_COX,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
//#define	cox2dep_is(s,v,u,w,p)    distr_IS(DIS_COX,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
//#define	cox2fun_is(s,f,g,h)	    distr_IS(DIS_COX,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)

#define	trival_is(s,v,u,w)	    distr_IS(DIS_TRI,s,DEP_NO,v,u,w,0, NULL,d_dum,d_dum,d_dum,d_dum)
#define	tridep_is(s,v,u,w,p)    distr_IS(DIS_TRI,s,DEP_PL,v,u,w,0, p,   d_dum,d_dum,d_dum,d_dum)
#define	trifun_is(s,f,g,h)	    distr_IS(DIS_TRI,s,DEP_MK,0,0,0,0, NULL,f,g,h,d_dum)


#define probval_is(s,v)        prob_IS(s,DEP_NO,v,NULL,d_dum)
#define probdep_is(s,v,p)      prob_IS(s,DEP_PL,v,  p, d_dum)
#define probfun_is(s,f)        prob_IS(s,DEP_MK,0,NULL,f)



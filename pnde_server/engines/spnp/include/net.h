/* net.h */
/* This file contains the prototypes of the user defined functions */

void		options(void);
void		net(void);
int		assert(void);
void		ac_init(void);
void		ac_reach(void);
void		ac_final(void);

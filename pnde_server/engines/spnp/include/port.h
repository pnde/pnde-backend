/* port.h */

/*
# ifdef vms
# define	OK_CODE	1
# define	ER_CODE 8
# endif

# ifdef unix
*/
# define	OK_CODE 0
# define	ER_CODE 1

#ifdef PlatformSUN
	extern int fprintf(FILE *stream, const char *format, ...);
	extern int fscanf(FILE *stream, const char *format, ...);
	extern int printf(const char *format, ...);
	extern int scanf(const char *format, ...);
	/*extern char *sprintf(char *s, const char *format, ...);*/
	extern int sscanf(const char *s, const char *format, ...);
	extern int fclose(FILE *stream);
	extern int _filbuf (FILE *);
#endif

#include "user.h"

int chargeRate = 2;
int sendConsume = 3;
int processConsume = 2;
int bufferSize = 0;

void options()
{
    iopt(IOP_SSMETHOD,VAL_SSSOR);
    iopt(IOP_PR_FULL_MARK, VAL_YES);
    iopt(IOP_PR_MARK_ORDER, VAL_CANONIC);
    iopt(IOP_PR_MC_ORDER, VAL_TOFROM);
    iopt(IOP_PR_MC, VAL_YES);
    iopt(IOP_MC, VAL_CTMC);
    iopt(IOP_PR_PROB, VAL_YES);
    iopt(IOP_PR_RSET, VAL_YES);
    iopt(IOP_PR_RGRAPH, VAL_YES);
    iopt(IOP_ITERATIONS, 200);
    iopt(IOP_CUMULATIVE, VAL_NO);
    fopt(FOP_ABS_RET_M0, 0.0);
    fopt(FOP_PRECISION, 0.00001);

    /*bufferSize = input("Enter buffer Size: ");
    inputRate = finput("Enter inputRate: ")*/
}

int chargeEnabled() {
     if(mark("Source") + chargeRate <=100)
        return 1;
     else
        return 0;
}

int RecivedChargeEnabled() {
     if(mark("Source") + 1 <=100)
        return 1;
     else
        return 0;
}

int RecivedChargeDisable() {
     if(mark("Source") > 99)
        return 1;
     else
        return 0;
}

int getEnabled() {
     if(mark("Input")  >20 )
        return 0;
     else
        return 1;
}

int processEnabled() {
     if(mark("ReadyForProcess")  >50 )
        return 0;
     else
        return 1;
}

int sendEnabled() {
     if(mark("ReadyForSend")  >20 )
        return 0;
     else
        return 1;
}

void net()
{
    place("Input");
    place("ReadyForProcess");
    place("ReadyForSend");
    place("Source");

    place("intermediateRecive");

    init("Source", 50);

    rateval("Get", 0.1);
    rateval("Recive", 1);
    rateval("ProcessTR", 10);
    rateval("Send", 1);
    rateval("Charge", 1);
    imm("t1");
    imm("t2");    


    iarc("Recive", "Input");
    iarc("ProcessTR", "ReadyForProcess");
    iarc("Send", "ReadyForSend");
    oarc("Recive", "ReadyForProcess");
    oarc("Get", "Input");
    oarc("ProcessTR", "ReadyForSend");


    oarc("Recive", "intermediateRecive");
    iarc("t1", "intermediateRecive");
    iarc("t2", "intermediateRecive");
    oarc("t1", "Source");

    // after convert

    moarc("Charge", "Source", chargeRate);
    miarc("Send", "Source", sendConsume);
    miarc("ProcessTR", "Source", processConsume);

    guard("Charge", chargeEnabled);
    guard("Get", getEnabled);
    guard("Recive", processEnabled);
    guard("ProcessTR", sendEnabled);
    guard("t1", RecivedChargeEnabled);
    guard("t2", RecivedChargeDisable);

    priority("t1", 10);
    priority("t2", 10);
    // END

}

int assert()
{
    /*printf("Input %i ",mark("Input"));
    printf("ReadyForProcess %i ",mark("ReadyForProcess"));
    printf("ReadyForSend %i ",mark("ReadyForSend"));
    printf("Source %i ",mark("Source"));*/
    bufferSize = bufferSize+1;
    //printf("bufferSize %i\n",mark("intermediateRecive"));
    //bufferSize = input("Enter buffer Size: ");
    if (mark("Source") > 100)
        return (RES_NOERR);
    return (RES_NOERR);
}

void ac_init()
{
    pr_net_info();
}

void ac_reach()
{
    pr_rg_info();
}

/*reward rate */
double Input() { return ((double)mark("Input")); }
double ReadyForProcess() { return ((double)mark("ReadyForProcess")); }
double ReadyForSend() { return ((double)mark("ReadyForSend")); }
double Source() { return ((double)mark("Source")); }


void ac_final()
{
    solve(INFINITY);
    pr_mc_info();
    pr_expected("mark(Input)", Input);
    pr_expected("mark(ReadyForProcess)", ReadyForProcess);
    pr_expected("mark(ReadyForSend)", ReadyForSend);
    pr_expected("rate(Source)", Source);
    pr_std_average();
}
# Makefile to run spnp when testing splitting
#
CC = gcc -c -g -DDEBUG_FLAG=0
LD = gcc  -g
SPNPDIR = $(SPNP_DIRECTORY)
all: $(SPN).spn
	@ echo SPNP Version 6.1
	@ echo The analysis is starting.
	@ .\\$(SPN).spn $(SPN)
$(SPN).spn: $(SPNPDIR)\lib\libspnp6_mingw.a $(SPN).o
	del /F $(SPN).spn
	$(LD) -o $(SPN).spn $(SPN).o -lspnp6_mingw  -lm

$(SPN).spnb: $(SPNPDIR)\lib\libspnp6_bruno.a $(SPN).o
	del /F $(SPN).spn
	$(LD) -o $(SPN).spn $(SPN).o -L"$(SPNPDIR)\lib" -lspnp6_bruno

$(SPN).spnc: $(SPNPDIR)\lib\libspnp6_bruno.a $(SPN).o
	del /F $(SPN).spn
	$(LD) -o $(SPN).spn $(SPN).o -L"$(SPNPDIR)\lib" -lspnp5

$(SPN).o: $(SPN).c $(SPNPDIR)\include\user.h $(SPNPDIR)\include\const.h $(SPNPDIR)\include\type.h $(SPNPDIR)\include\port.h
	$(CC) -I"$(SPNPDIR)\include" -I. $(SPN).c
lint:
	lint -v -I$(SPNPDIR)\include $(SPNPDIR)\src\cspl_stub.c $(SPN).c

SPNP V6.0 in Win98/NT/2000/Me
-----------------------------

This is the Microsoft Windows version of SPNP v6.0.  The package contains the following directories and files:

Readme.txt:  this file.
init_spnp.bat:  initialization batch file.
Makerun.dos:  a makefile needed to run spnp.
bin\:  binary files for gcc, gdb, spnp.bat, etc.
include\: include files for SPNP.
lib\:  library files for SPNP and GCC.
i386-mingw32\:  GCC-2.95.2 Mingw distribution files.
example\: SPNP exmaples.
doc\: SPNP manuals and other documents

============
Installation
============

1. Unpack the spnp6_w32.zip into a directory for SPNP (eg. c:\spnp).  It contains the gcc compiler and SPNP libraries. 
2. Edit the init_spnp.bat to make the SPNP_DIRECTORY environment variable to be the install directory you choose.
3. Run initialization script to setup environmental variables:
	C:\SPNP>init_spnp.bat
4. Now spnp is ready to run.  You can go to example directory and run:
        C:\SPNP\EXAMPLE> spnp example1
This example should be executed and display the results. 
5. You can further move the stuff in init_spnp.bat into 
autoexec.bat or set up the variables in Control Panel.
--------------------------------------------------------

import os.path
import subprocess

from django.conf import settings
from django.template.loader import render_to_string


class SPNPCommandFailed(Exception):
    pass


class SPNPRunner:
    def __init__(self, petri_model):
        self.model = petri_model
        self.nodes = petri_model.model_data["nodes"]
        self.edges = petri_model.model_data["edges"]
        self.options = petri_model.options
        self.user_code = petri_model.extra_code
        self.extra_code = []
        self.model_id = petri_model.id
        self.extra_net_code = []

        self._add_edges_multiplicity_functions()
        self.model_pre_process()
        self.label_mappers = {}

        for node in self.nodes:
            label = node["data"]["label"]
            if label:
                self.label_mappers[node["id"]] = label
                node["id"] = label

    def model_pre_process(self):
        self._source_base_pre_process()
        self._location_base_pre_process()

    def _location_base_pre_process(self):
        if self.model.model_type not in [
            self.model.MODEL_TYPE_LOCATION,
            self.model.MODEL_TYPE_LOCATION_SOURCE,
        ]:
            return

        for tr in self.model.location_related_transitions:
            conditions = []
            for location_id, location_deps in tr["location_depends_type"].items():
                if location_deps["type"].upper() == "N":
                    continue
                operator = "!=" if location_deps["type"].lower() == "d" else "=="
                conditions += [
                    f'mark("{location_id}") {operator} {value}'
                    for value in location_deps["set"].split(",")
                ]
                guard_function = (
                    f"int guard_of_{tr['id']} () {{" f'{" && ".join(conditions)}' f"}}"
                )
                tr["guard_mode"] = "have_guard"
                tr["guard_function"] = guard_function
                if capacity := self.model.find_node(location_id).get("capacity"):
                    guard_func_name = f"{location_id}_capacity_guard"
                    self.extra_code.append(
                        f"int {guard_func_name}() {{ "
                        f'return mark("{location_id}") <= {capacity};'
                        f"}}"
                    )
                    self.extra_net_code.append(
                        f'\n\tguard("{tr["id"]}", &{guard_func_name});'
                    )

    def _source_base_pre_process(self):
        ts_count = 0
        ps_count = 0
        if self.model.model_type not in [
            self.model.MODEL_TYPE_SOURCE,
            self.model.MODEL_TYPE_LOCATION_SOURCE,
        ]:
            return

        for tr in self.model.source_related_transitions:
            for source_id, source_rate in tr["source_rates"].items():
                source = self.model.find_node(source_id)
                source_rate = int(source_rate)
                if source_rate < 0:
                    edge = self.model.create_edge(source_id, tr["id"], source_rate * -1)
                    self.edges.append(edge)
                elif source_rate > 0 and self.model.node_have_outgoing_link(tr["id"]):
                    new_place_id = f"PS{ps_count}"
                    new_place = self.model.create_place(new_place_id)
                    ps_count += 1
                    self.nodes.append(new_place)

                    new_im_tr_id_a = f"TS{ts_count}"
                    ts_count += 1
                    guard_function = (
                        f"int guard_of_{new_im_tr_id_a} () {{ "
                        f'(mark("{source_id}") + {source_rate} <= {source["capacity"]}); '
                        f"}}"
                    )
                    new_im_tr_a = self.model.create_transition(
                        tr_id=new_im_tr_id_a,
                        tr_type="immTransition",
                        guard_function=guard_function,
                    )
                    self.nodes.append(new_im_tr_a)
                    edge = self.model.create_edge(
                        new_place_id, new_im_tr_id_a, source_rate
                    )
                    self.edges.append(edge)

                    new_im_tr_id_b = f"TS{ts_count}"
                    ts_count += 1
                    guard_function = (
                        f"int guard_of_{new_im_tr_id_b} () {{ "
                        f'(mark("{source_id}") + {source_rate} > {source["capacity"]}); '
                        f"}}"
                    )
                    new_im_tr_b = self.model.create_transition(
                        tr_id=new_im_tr_id_b,
                        tr_type="immTransition",
                        guard_function=guard_function,
                    )
                    self.nodes.append(new_im_tr_b)
                    edge = self.model.create_edge(new_place_id, new_im_tr_id_b)
                    self.edges.append(edge)

                    edge = self.model.create_edge(
                        new_im_tr_id_a, source_id, source_rate
                    )
                    self.edges.append(edge)
                elif source_rate > 0:
                    edge = self.model.create_edge(tr["id"], source_id, source_rate)
                    self.edges.append(edge)
                    guard_func_name = f"{source_id}_capacity_quard"
                    self.extra_code.append(
                        f"int {guard_func_name}() {{ "
                        f'return mark("{source_id}") + {source_rate} <= {source["capacity"]};'
                        f"}}"
                    )
                    self.extra_net_code.append(
                        f'\n\tguard("{tr["id"]}", &{guard_func_name});'
                    )

    @property
    def options_function_calls(self):
        result = []
        for option in self.options:
            if self.options[option]["checked"] is False:
                continue
            option_data = self.options[option]
            result.append(
                f"\t{option_data['function']}({option}, {option_data['value']});\n"
            )
        if result:
            return ["\n"] + result
        return result

    @property
    def places_net_function_calls(self):
        places = list(
            filter(
                lambda x: x["type"].lower() in ["place", "source", "location"],
                self.nodes,
            )
        )
        result = ["\n\t// places"]
        for place in places:
            result += [
                f'\n\tplace("{place["id"]}");',
            ]
            token = place["data"]["petri_token"]
            if token > 0:
                result += [
                    f'\n\tinit("{place["id"]}", {token});',
                ]
            # result.append("\n")

        return result

    def get_common_transition_definition_code(self, tr):
        result = []
        if tr["priority"] != 0:
            result.append(f'\n\tpriority("{tr["id"]}", {tr["priority"]});')
        if tr["policy"].lower() != "prd":
            result.append(f'\n\tpolicy("{tr["id"]}", {tr["policy"]});')
        if tr["guard_mode"] == "have_guard":
            result.append(f'\n\tguard("{tr["id"]}", &guard_of_{tr["id"]});')
            self.extra_code.append(tr["guard_function"])
        return result

    @staticmethod
    def _get_function_name(func_code):
        return func_code.split("(")[0].split()[1]

    @property
    def timed_transitions_net_function_calls(self):
        places = list(
            filter(
                lambda x: x["type"].lower()
                in [
                    "timedtransition",
                    "sourcetimedtransition",
                    "locationtimedtransition",
                ],
                self.nodes,
            )
        )
        result = []
        for tr in places:
            print(tr)
            if tr["rate_mode"] != "use_provided_function":
                result += [f'\n\trateval("{tr["id"]}", {tr["rate"]});']
            else:
                result += [f'\n\tratefun("{tr["id"]}", rate_of_{tr["id"]});']
                self.extra_code.append(tr["rate_function_code"])
            result += self.get_common_transition_definition_code(tr)
        return ["\n\t// timed transitions"] + result + ["\n"]

    @property
    def immediate_transitions_net_function_calls(self):
        places = list(
            filter(
                lambda x: x["type"].lower()
                in ["immtransition", "sourceimmtransition", "locationimmtransition"],
                self.nodes,
            )
        )
        result = []
        for tr in places:
            result += [f'\n\timm("{tr["id"]}");']
            if tr["rate_mode"] == "use_provided_function":
                result += [f'\n\tprobfun("{tr["id"]}", rate_of_{tr["id"]});']
                self.extra_code.append(tr["rate_function_code"])
            elif float(tr["rate"]) > 1:
                result += [f'\n\tprobval("{tr["id"]}", {tr["rate"]});']
            result += self.get_common_transition_definition_code(tr)
        return ["\n\t// immediate transitions"] + result

    def _add_edges_multiplicity_functions(self):
        for edge in self.edges:
            if edge.get("has_multiplicity"):
                self.extra_code.append(edge["multiplicity_func"])

    @property
    def edge_definitions(self):
        result = ["\n\t// edges definitions"]

        for edge in self.edges:
            source = self.label_mappers.get(edge["source"], edge["source"])
            target = self.label_mappers.get(edge["target"], edge["target"])
            if edge.get("is_inhibitor"):
                command = f'harc("{source}", "{target}"'
            elif edge["source"][0] == "P":
                command = f'iarc("{target}", "{source}"'
            else:
                command = f'oarc("{source}", "{target}"'

            m = edge.get("multiplicity", 0)
            if edge.get("has_multiplicity"):
                command = (
                    f'v{command}, {self._get_function_name(edge["multiplicity_func"])}'
                )
            elif int(m) > 1:
                command = f"m{command}, {m}"
            result.append(f"\n\t{command});")
        return result

    @property
    def code_context(self):
        return {
            "options": self.options_function_calls,
            "net": (
                *self.places_net_function_calls,
                *self.timed_transitions_net_function_calls,
                *self.immediate_transitions_net_function_calls,
                *self.edge_definitions,
                *self.extra_net_code,
            ),
            "user_code": self.user_code,
            "extra_code": self.extra_code,
        }

    @property
    def code(self):
        return render_to_string("code/spnp.c", self.code_context)

    @staticmethod
    def clean_outputs(outputs):
        result = []
        for index, output in enumerate(outputs):
            output = output.replace("\n", "").replace("\t", "")
            output = " ".join(output.split())
            if output and output[-1] == ":":
                output = output.replace(":", "")

            if output:
                result.append(output)

        return result

    @staticmethod
    def convert_outputs_to_dict(cleaned_outputs):
        result = {}
        property_name = ""
        for output in cleaned_outputs:
            if output[0] == "=" and property_name:
                continue
            elif output[0] == "=":
                property_name = ""
            elif output[0:6].lower() == "time :":
                result["time"] = output.replace("TIME : ", "")
                property_name = ""
            elif output.isupper():
                property_name = output.lower()
            else:
                result[property_name] = result.get(property_name, [])
                result[property_name].append(output)

        for item in ("net", "rg", "ctmc"):
            items = []
            for row in result.get(item, []):
                items.append(row.split(": "))
            result[item] = items

        avg_items = [
            item.replace(":", "").split() for item in result.get("average", [])
        ]
        for index, item in enumerate(avg_items):
            if item[0].lower() == "transition":
                places = avg_items[0:index]
                transitions = avg_items[index::]
                result["average"] = {
                    "places": places or [],
                    "transitions": transitions or [],
                }
                break

        return result

    def run(self):
        c_file_name = f"model_{self.model_id}"
        c_source_code_file_path = os.path.join(settings.ROOT_DIR, "c_source_codes")
        os.makedirs(c_source_code_file_path, exist_ok=True)

        spnp_dir = os.path.join(settings.APPS_DIR, "engines", "spnp")

        exec_env = os.environ.copy()

        exec_env[
            "PATH"
        ] = f'{exec_env["PATH"]};{spnp_dir};{os.path.join(spnp_dir, "bin")}'

        exec_env["SPNP_DIRECTORY"] = spnp_dir

        with open(
            os.path.join(c_source_code_file_path, f"{c_file_name}.c"), "w"
        ) as source_file:
            source_file.write(self.code)

        result = subprocess.run(
            ["spnp", c_file_name],
            cwd=c_source_code_file_path,
            env=exec_env,
            text=True,
            shell=True,
            capture_output=True,
        )
        if result.returncode != 0:
            raise SPNPCommandFailed(result.stderr)

        result = result.stdout
        with open(
            os.path.join(c_source_code_file_path, f"{c_file_name}.log")
        ) as log_file:
            logs = log_file.readlines()

        with open(
            os.path.join(c_source_code_file_path, f"{c_file_name}.out")
        ) as out_file:
            outputs = out_file.readlines()

        raw_output = outputs
        outputs = self.clean_outputs(outputs)
        outputs = self.convert_outputs_to_dict(outputs)

        return {
            "outputs": outputs,
            "execution_output": result,
            "logs": " ".join(logs),
            "raw_output": raw_output,
        }

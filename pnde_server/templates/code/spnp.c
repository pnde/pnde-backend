#include "user.h"

void options() { {% for option in options %}{{ option|safe }}{% endfor %} }

{% for code in extra_code%}
{{ code|safe }}
{% endfor %}

void net() { {% for net_call in net%} {{ net_call|safe }} {% endfor %}
}

{{ user_code|safe }}

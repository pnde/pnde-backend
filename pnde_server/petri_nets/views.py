from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..engines.spnp import SPNPRunner
from .models import PetriNet
from .serializers import PetriNetSerializer


class PetriNetViewSet(ModelViewSet):
    # pagination_class = LimitOffsetPagination
    permission_classes = (IsAuthenticated,)
    serializer_class = PetriNetSerializer

    def get_queryset(self):
        return PetriNet.objects.filter(user=self.request.user)

    @action(detail=True)
    def spnp_code(self, request, *args, **kwargs):
        petri_model = self.get_object()
        runner = SPNPRunner(petri_model=petri_model)
        return Response({"code": runner.code})

    @action(detail=True)
    def run_spnp(self, request, *args, **kwargs):
        petri_model = self.get_object()
        runner = SPNPRunner(petri_model=petri_model)
        return Response(runner.run())

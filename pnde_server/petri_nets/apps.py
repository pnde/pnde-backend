from django.apps import AppConfig


class PetriNetsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pnde_server.petri_nets"

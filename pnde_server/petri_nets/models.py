from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

SPNP_EXTRA_CODE_DEFAULT = """
int assert() {
    return(RES_NOERR);
}

void ac_init() {
    pr_net_info();
}

void ac_reach() {
    pr_rg_info();
}

void ac_final() {
    // define your final function here
    solve(INFINITY);
    pr_std_average();
}
"""


class NodeNotFound(Exception):
    pass


class PetriNet(models.Model):
    MODEL_TYPE_BASIC = 10
    MODEL_TYPE_SOURCE = 20
    MODEL_TYPE_LOCATION = 30
    MODEL_TYPE_LOCATION_SOURCE = 40
    MODEL_TYPE_CHOICES = (
        (MODEL_TYPE_BASIC, _("basic")),
        (MODEL_TYPE_SOURCE, _("source")),
        (MODEL_TYPE_LOCATION, _("location")),
        (MODEL_TYPE_LOCATION_SOURCE, _("location source")),
    )

    ENGINE_NO_ENGINE = 10
    ENGINE_SPNP = 20
    ENGINE_TYPES = (
        (ENGINE_NO_ENGINE, _("no engine")),
        (ENGINE_SPNP, _("spnp")),
    )

    model_type = models.PositiveSmallIntegerField(
        default=MODEL_TYPE_BASIC, choices=MODEL_TYPE_CHOICES
    )

    engine = models.PositiveSmallIntegerField(default=ENGINE_SPNP, choices=ENGINE_TYPES)
    name = models.CharField(verbose_name=_("name"), max_length=255)
    model_data = models.JSONField(default=dict, verbose_name=_("model"), blank=True)
    options = models.JSONField(default=dict, verbose_name=_("options"), blank=True)
    extra_code = models.TextField(
        default=SPNP_EXTRA_CODE_DEFAULT, verbose_name=_("extra code"), blank=True
    )
    user = models.ForeignKey(
        get_user_model(),
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("name", "user", "model_type")
        index_together = (("user", "name"),)

    def __str__(self):
        return f"{self.user}:{self.name}"

    @property
    def sources(self):
        if self.model_type != self.MODEL_TYPE_SOURCE:
            return []
        sources = []
        for node in self.model_data["node"]:
            if node["type"].lower() == ["source"]:
                sources.append(node)
        return sources

    @property
    def source_related_transitions(self):
        if self.model_type != self.MODEL_TYPE_SOURCE:
            return []
        transitions = []
        for node in self.model_data["nodes"]:
            if node["type"].lower() in ["sourcetimedtransition", "sourceimmtransition"]:
                transitions.append(node)
        return transitions

    @property
    def location_related_transitions(self):
        if self.model_type != self.MODEL_TYPE_LOCATION:
            return []
        transitions = []
        for node in self.model_data["nodes"]:
            if node["type"].lower() in [
                "locationtimedtransition",
                "locationimmtransition",
            ]:
                transitions.append(node)
        return transitions

    def find_node(self, node_id):
        for node in self.model_data["nodes"]:
            if node["id"] == node_id:
                return node
        raise NodeNotFound

    def node_have_outgoing_link(self, node_id):
        for edge in self.model_data["edges"]:
            target = self.find_node(edge["target"])
            if edge["source"] == node_id and target["type"].lower() != "place":
                return True
        return False

    @staticmethod
    def create_edge(source, target, multiplicity=0):
        return {"source": source, "target": target, "multiplicity": multiplicity}

    @staticmethod
    def create_place(place_id, tokens=0):
        return {"id": place_id, "data": {"petri_token": tokens}}

    @staticmethod
    def create_transition(tr_id, tr_type, guard_function):
        if guard_function:
            guard_mode = "have_guard"
        else:
            guard_mode = "no_guard"
        return {
            "id": tr_id,
            "type": tr_type,
            "guard_mode": guard_mode,
            "guard_function": guard_function,
        }

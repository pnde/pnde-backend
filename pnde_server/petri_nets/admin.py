from django.contrib import admin

from .models import PetriNet


@admin.register(PetriNet)
class PetriAdmin(admin.ModelAdmin):
    fields = (
        "model_data",
        "engine",
        "name",
        "options",
        "extra_code",
        "user",
        "model_type",
        "created_at",
        "modified_at",
    )
    list_display = (
        "id",
        "name",
        "engine",
        "user",
        "model_type",
        "created_at",
        "modified_at",
    )
    readonly_fields = (
        "created_at",
        "modified_at",
    )

from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import PetriNet


class PetriNetSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        write_only=True,
        default=serializers.CurrentUserDefault(),
        queryset=get_user_model().objects.all(),
    )
    model_type_display = serializers.CharField(
        source="get_model_type_display", read_only=True
    )
    engine_display = serializers.CharField(source="get_engine_display", read_only=True)

    class Meta:
        model = PetriNet
        fields = (
            "id",
            "created_at",
            "modified_at",
            "model_data",
            "options",
            "name",
            "extra_code",
            "user",
            "model_type",
            "engine",
            "model_type_display",
            "engine_display",
        )
        read_only_fields = (
            "created_at",
            "modified_at",
            "model_type_display",
            "engine_display",
        )
